<?php

use Ox3a\Form\Factory\ElementFactory;
use \PHPUnit\Framework\TestCase;
use Ox3a\Form\Model;

class CollectionTest extends TestCase
{
    /**
     * @param $data
     * @param $expected
     * @dataProvider dataProvider1
     */
    public function test1($data, $expected)
    {
        $form = $this->getForm();

        $form->setData([
            'product' => [
                ['name' => 'name', 'price' => 'ss'],
            ],
        ]);

        $form->setData($data);

        $result = $form->isValid();

        if ($expected) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }


    /**
     * @param $data
     * @param $expected
     * @dataProvider dataProvider2
     */
    public function test2($data, $expected)
    {
        $form = new Model\FormModel();

        $form->add([
            'name'    => 'name',
            'type'    => Model\MultiCheckboxModel::class,
            'options' => [
                'options' => [1, 2, 3, 4, 5],
            ],
        ]);

        $form->setData($data);

        $result = $form->isValid();

        if ($expected) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }


    public function dataProvider1()
    {
        return [
            [[], true],
            [['form' => 1], true],
            [['product' => []], true],
            [['product' => [['name' => 'name']]], false],
            [['product' => [['name' => 'name', 'price' => 'price']]], true],
            [['product' => [
                ['name' => 'name', 'price' => 'price'],
                ['name' => 'name', 'price' => ''],
            ]], false],
        ];
    }


    public function dataProvider2()
    {
        return [
            [[], true],
            ['', true],
            [['name' => [5, 6, 10]], false],
            [['name' => '5, 6, 10'], false],
            [['name' => [0, 1]], true],
            [['name' => '0,1'], true],
        ];
    }


    public function getForm()
    {
        $form = new Model\FormModel();

        /** @var Model\CollectionModel $collection */
        $collection = ElementFactory::factory([
            'name' => 'product',
            'type' => Model\CollectionModel::class,
        ]);

        $collection
            ->add([
                'name'       => 'name',
                'type'       => Model\ElementModel::class,
                'attributes' => [
                    'required' => true,
                ],
            ])
            ->add([
                'name'       => 'price',
                'type'       => Model\ElementModel::class,
                'attributes' => [
                    'required' => true,
                ],
            ]);

        return $form
            ->add($collection)
            ->add([
                'name' => 'form',
            ]);
    }
}
