<?php

use PHPUnit\Framework\TestCase;
use Ox3a\Form\Model;

class SubFormTest extends TestCase
{
    /**
     * @param array $data
     * @param bool  $expected
     * @dataProvider dataProvider1
     */
    public function test1($data, $expected)
    {
        $form = $this->getForm();

        $form->setData($data);
        $result = $form->isValid();

        if ($expected) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }


    public function dataProvider1()
    {
        return [
            [[], false],
            [['name' => 1], false],
            [['name' => 1, 'sub' => []], false],
            [['name' => 1, 'sub' => ['name' => '']], false],
            [['name' => 1, 'sub' => ['name' => '2']], true],
            [['name' => '', 'sub' => ['name' => '2']], false],
        ];
    }


    public function getForm()
    {

        $form = new Model\FormModel();

        $form
            ->add([
                'name'       => 'name',
                'type'       => Model\ElementModel::class,
                'attributes' => [
                    'required' => true,
                ],
            ])
            ->add([
                'name' => 'sub',
                'type' => Model\GroupModel::class,
            ]);
        $form->getElement('sub')->add([
            'name'       => 'name',
            'type'       => Model\ElementModel::class,
            'attributes' => [
                'required' => true,
            ],
        ]);
        return $form;
    }
}
