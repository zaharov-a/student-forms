<?php


use Ox3a\Form\Factory\ElementFactory;
use Ox3a\Form\Validator\DigitsValidator;
use PHPUnit\Framework\TestCase;
use Ox3a\Form\Model;
use Zend\Filter;
use Zend\Validator;

class JsonTest extends TestCase
{

    /**
     * @param $params
     * @param $expected
     * @dataProvider dataProvider1
     */
    public function test1($params, $expected)
    {
        $element = ElementFactory::factory($params);

        $this->assertEquals($expected, json_decode(json_encode($element), true));
    }


    public function dataProvider1()
    {
        return [
            [
                [
                    'type'        => Model\ElementModel::class,
                    'name'        => 'text',
                    'validators'  => [
                        ['name' => Validator\Digits::class],
                        new DigitsValidator(),
                        ['name' => Validator\StringLength::class, 'min' => 1],
                    ],
                    'filters'     => [
                        ['name' => Filter\StringTrim::class],
                    ],
                    'placeholder' => 'placeholder',
                ],
                [
                    'name'            => 'text',
                    'value'           => null,
                    'attributes'      => [
                        "class" => "form-control",
                        "name"  => "text",
                        "id"    => "input_text",
                    ],
                    "options"         => [
                        "width" => "col-sm-10",
                    ],
                    "label"           => null,
                    "labelAttributes" => [],
                    "labelOptions"    => [
                        "width" => "col-sm-2",
                    ],
                    "validators"      => [
                        [
                            "name" => "Zend\\Validator\\Digits",
                        ],
                        [
                            "name" => "Zend\\Validator\\Digits",
                        ],
                        [
                            "name" => "Zend\\Validator\\StringLength",
                            "min"  => 1,
                        ],
                    ],
                    "filters"         => [
                        [
                            "name" => "Zend\\Filter\\StringTrim",
                        ],
                    ],
                ],
            ],
        ];
    }
}
