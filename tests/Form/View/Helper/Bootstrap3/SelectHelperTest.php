<?php

namespace Form\View\Helper\Bootstrap3;

use Ox3a\Form\Factory\ElementFactory;
use Ox3a\Form\View\Helper\Bootstrap3\SelectHelper;
use PHPUnit\Framework\TestCase;

class SelectHelperTest extends TestCase
{
    public function testRender()
    {
        $helper = new SelectHelper();

        $select = ElementFactory::factory([
            'name'       => "select1",
            'attributes' => [
            ],
            'options'    => [
                'label'      => "Тип",
                'options'    => [
                    1 => 1,
                    2 => 2,
                ],
                'escapeAttr' => true,
            ],
            'type'       => \Ox3a\Form\Model\SelectModel::class,
        ]);

        $this->assertEquals(
            '<select class="form-control" name="select1" id="input_select1"><option value="1">1</option><option value="2">2</option></select>',
            (string)$helper($select)
        );

        $this->assertEquals(
            '<select class="form-control" name="select1" id="input_select1"><option value="1" selected="selected">1</option><option value="2">2</option></select>',
            (string)$helper($select->setValue(1))
        );

        $selectOptGroup = ElementFactory::factory([
            'name'       => "select1",
            'attributes' => [
            ],
            'options'    => [
                'label'      => "Тип",
                'options'    => [
                    'group1' => [
                        1 => 1,
                        2 => 2,
                    ],
                    'group2' => [
                        3 => 1,
                        4 => 2,
                    ],
                ],
                'escapeAttr' => true,
            ],
            'type'       => \Ox3a\Form\Model\SelectModel::class,
        ]);

        $this->assertEquals(
            '<select class="form-control" name="select1" id="input_select1"><optgroup label="group1"><option value="1">1</option><option value="2">2</option></optgroup><optgroup label="group2"><option value="3">1</option><option value="4">2</option></optgroup></select>',
            (string)$helper($selectOptGroup)
        );

        $this->assertEquals(
            '<select class="form-control" name="select1" id="input_select1"><optgroup label="group1"><option value="1">1</option><option value="2">2</option></optgroup><optgroup label="group2"><option value="3" selected="selected">1</option><option value="4">2</option></optgroup></select>',
            (string)$helper($selectOptGroup->setValue(3))
        );

        $selectMixed = ElementFactory::factory([
            'name'       => "select1",
            'attributes' => [
            ],
            'options'    => [
                'label'      => "Тип",
                'options'    => [
                    'group1' => [
                        1 => 1,
                        2 => 2,
                    ],
                    3        => 1,
                    4        => 2,
                ],
                'escapeAttr' => true,
            ],
            'type'       => \Ox3a\Form\Model\SelectModel::class,
        ]);

        $this->assertEquals(
            '<select class="form-control" name="select1" id="input_select1"><optgroup label="group1"><option value="1">1</option><option value="2">2</option></optgroup><option value="3" selected="selected">1</option><option value="4">2</option></select>',
            (string)$helper($selectMixed->setValue(3))
        );
    }
}
