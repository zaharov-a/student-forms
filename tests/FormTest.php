<?php


use Ox3a\Form\Model\FormModel;
use Ox3a\Form\Model\GroupModel;
use PHPUnit\Framework\TestCase;

class FormTest extends TestCase
{
    /**
     * @param $required
     * @param $value
     * @param $expected
     * @dataProvider dataProvider1
     */
    public function test1($required, $value, $expected)
    {
        $form   = new FormModel();
        $config = $form->getDefaultConfigGenerator();

        $form->add($config->getText(['name' => 'name']));
        $group = new GroupModel('group');
        $group->add($config->getRequiredDate(['name' => 'date']));
        $form->add($group);
        $form->getElement('name')->setRequired($required);

        $form->setValue(['name' => $value, 'group' => ['date' => '2020-02-02']]);

        if (!$expected) {
            $this->assertFalse($form->isValid());
        } else {
            $this->assertTrue($form->isValid());
        }
    }


    public function dataProvider1()
    {
        return [
            [true, '', false],
            [true, 'value', true],
            [false, '', true],
            [false, 'value', true],
        ];
    }

}
