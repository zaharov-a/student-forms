<?php

namespace Ox3a\Annotation\Form\Validator;

use Ox3a\Annotation\Form\ValidatorBuilderInterface;
use Zend\Validator\GreaterThan;

/**
 * @Annotation
 */
class GreaterThanValidator implements ValidatorBuilderInterface
{

    /**
     * @var int|float
     */
    private $min;

    /**
     * @var bool
     */
    private $inclusive = false;

    public function __construct($data)
    {
        if (isset($data['value'])) {
            $data['min'] = $data['value'];
        }
        $this->min = $data['min'];

        if (isset($data['inclusive'])) {
            $this->inclusive = $data['inclusive'];
        }
    }

    public function __toString()
    {
        $template = $this->inclusive
            ? "new \\%s(['min' => %s, 'inclusive' => true])"
            : "new \\%s(['min' => %s])";

        return sprintf(
            $template,
            GreaterThan::class,
            $this->min
        );
    }

}
