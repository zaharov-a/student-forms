<?php

namespace Ox3a\Annotation\Form\Validator;

use Ox3a\Annotation\Form\ValidatorBuilderInterface;

/**
 * @Annotation
 */
class NotInCallbackValidator implements ValidatorBuilderInterface
{
    /**
     *
     * @var string
     */
    private $method;

    public function __construct($data)
    {
        if (isset($data['value'])) {
            $data['method'] = $data['value'];
        }

        $this->method = $data['method'];
    }

    public function __toString()
    {
        return sprintf(
            "new \\%s(\$this->getOption('%s'))",
            \Ox3a\Form\Validator\NotInCallbackValidator::class,
            $this->method
        );
    }

}
