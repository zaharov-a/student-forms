<?php

namespace Ox3a\Annotation\Form\Validator;

use Ox3a\Annotation\Form\ValidatorBuilderInterface;
use Zend\Validator\LessThan;

/**
 * @Annotation
 */
class LessThanValidator implements ValidatorBuilderInterface
{

    /**
     * @var int|float
     */
    private $max;

    /**
     * @var bool
     */
    private $inclusive = false;

    public function __construct($data)
    {
        if (isset($data['value'])) {
            $data['max'] = $data['value'];
        }
        $this->max = $data['max'];

        if (isset($data['inclusive'])) {
            $this->inclusive = $data['inclusive'];
        }
    }

    public function __toString()
    {
        $template = $this->inclusive
            ? "new \\%s(['max' => %s, 'inclusive' => true])"
            : "new \\%s(['max' => %s])";

        return sprintf(
            $template,
            LessThan::class,
            $this->max
        );
    }

}
