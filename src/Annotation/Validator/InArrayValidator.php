<?php

namespace Ox3a\Annotation\Form\Validator;

use Ox3a\Annotation\Form\ValidatorBuilderInterface;
use Zend\Validator\InArray;

/**
 * @Annotation
 */
class InArrayValidator implements ValidatorBuilderInterface
{
    /**
     * @var string
     */
    private $fromOption;

    /**
     * @var array
     */
    private $haystack;

    public function __construct($data)
    {
        if (isset($data['value'])) {
            $data['haystack'] = $data['value'];
        }

        $this->haystack = $data['haystack'];
        if (isset($data['fromOption'])) {
            $this->fromOption = $data['fromOption'];
        }
    }

    public function __toString()
    {
        return sprintf(
            "new \\%s(['max' => %s])",
            InArray::class,
            $this->fromOption
                ? "\$this->getOption('{$this->fromOption}')"
                : var_export($this->haystack, true)
        );
    }

}
