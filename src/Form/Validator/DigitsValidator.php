<?php


namespace Ox3a\Form\Validator;


use JsonSerializable;
use Zend\Validator\Digits;

class DigitsValidator extends Digits implements JsonSerializable
{

    /**
     * Specify data which should be serialized to JSON
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'name' => parent::class,
        ];
    }
}
