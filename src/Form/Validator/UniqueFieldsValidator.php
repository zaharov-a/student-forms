<?php


namespace Ox3a\Form\Validator;

use Zend\Validator\ValidatorInterface;

class UniqueFieldsValidator implements ValidatorInterface
{

    const ERROR_RECORD_FOUND = 'recordFound';

    /**
     * @var AbstractMapper
     */
    protected $_mapper;
    protected $_fields;
    protected $_id;

    protected $_messages = [];

    protected $_messageTemplates = [
        self::ERROR_RECORD_FOUND => 'Найдена совпадающая со значением запись',
    ];


    public function __construct($options)
    {
        $this->_mapper = $options['mapper'];
        $this->_fields = $options['fields'];
        $this->_id     = $options['id'];
    }


    public function isValid($value, $context = null)
    {
        $fieldData = [];

        foreach ($this->_fields as $field) {
            $fieldData[$field] = isset($context[$field]) ? $context[$field] : null;
        }

        $result = call_user_func([$this->_mapper, 'checkUniqueFields'], $fieldData, $this->_id);

        if ($result) {
            $this->error(self::ERROR_RECORD_FOUND);
            return false;
        }

        return true;
    }


    public function getMessages()
    {
        return $this->_messages;
    }


    public function error($key)
    {
        $this->_messages[$key] = $this->_messageTemplates[$key];
    }

}
