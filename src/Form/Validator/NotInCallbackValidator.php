<?php

namespace Ox3a\Form\Validator;

use Zend\Validator\AbstractValidator;

class NotInCallbackValidator extends AbstractValidator
{

    const ERROR_RECORD_FOUND = 'pskRecordFound';

    /**
     * @var array Message templates
     */
    protected $messageTemplates = [
        self::ERROR_RECORD_FOUND => "A record matching the input was found",
    ];

    /**
     * @var callable in(value) => true
     */
    private $callback;

    /**
     * @param callable $callback
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
        parent::__construct();
    }

    public function isValid($value)
    {
        if (call_user_func($this->callback, $value)) {
            $this->error(self::ERROR_RECORD_FOUND);
            return false;
        }

        return true;
    }

}
