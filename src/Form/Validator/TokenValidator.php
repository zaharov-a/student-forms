<?php


namespace Ox3a\Form\Validator;


use Zend\Validator\Csrf;

class TokenValidator extends Csrf
{

    public function removeToken($hash = null)
    {
        if (!$hash) {
            return;
        }

        $tokenId = $this->getTokenIdFromHash($hash);
        $session = $this->getSession();
        if (isset($session->tokenList[$tokenId])) {
            unset($session->tokenList[$tokenId]);
        }
    }

}
