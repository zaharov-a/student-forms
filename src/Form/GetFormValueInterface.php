<?php

namespace Ox3a\Form;

/**
 * Интерфейс получения данных в формате формы
 */
interface GetFormValueInterface
{
    /**
     * Получить данные в формате формы
     * @return array|null
     */
    public function getFormValue();
}
