<?php


namespace Ox3a\Form\Model;


class StaticModel extends ElementModel
{
    protected $_attributes = [
        'class' => 'form-control-static',
    ];


    /**
     * @inheritDoc
     * @param mixed $value
     * @return $this|StaticModel
     */
    public function setValue($value)
    {
        // запрещаем обнулять значение
        if (is_null($value)) {
            return $this;
        }
        return parent::setValue($value);
    }
}

