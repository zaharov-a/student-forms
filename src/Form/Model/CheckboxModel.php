<?php


namespace Ox3a\Form\Model;

use Zend\Validator;

class CheckboxModel extends ElementModel
{

    protected $_options = [
        'checked_value'   => 1,
        'unchecked_value' => 0,
    ];

    protected $_attributes = [
        'type'    => 'checkbox',
        'checked' => false,
    ];


    public function setValue($value)
    {
        $this->setAttribute('checked', $this->getOption('checked_value') == $value);
        return parent::setValue($value);
    }


    public function getInputSpecification()
    {
        $data = parent::getInputSpecification();

        $data['validators'][] = [
            'name'    => Validator\InArray::class,
            'options' => [
                'haystack' => [
                    $this->getOption('checked_value'),
                    $this->getOption('unchecked_value'),
                ],
            ],
        ];

        return $data;
    }


    public function isChecked()
    {
        return $this->getAttribute('checked');
    }

}
