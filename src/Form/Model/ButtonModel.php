<?php


namespace Ox3a\Form\Model;


class ButtonModel extends ElementModel
{
    protected $_attributes = [
        'type' => 'submit',
    ];

}
