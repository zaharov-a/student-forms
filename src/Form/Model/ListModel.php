<?php


namespace Ox3a\Form\Model;


use Countable;
use Iterator;

class ListModel implements Iterator, Countable
{
    /**
     * @var ElementModel[]
     */
    protected $_items = [];


    public function current()
    {
        $node = current($this->_items);

        return $node ?: false;
    }


    public function next()
    {
        $node = next($this->_items);

        return $node ?: false;
    }


    public function key()
    {
        return key($this->_items);
    }


    public function valid()
    {
        return current($this->_items) !== false;
    }


    public function rewind()
    {
        reset($this->_items);
    }


    public function count()
    {
        return count($this->_items);
    }

}
