<?php


namespace Ox3a\Form\Model;


use Ox3a\Form\Validator\TokenValidator;

class TokenModel extends ElementModel
{
    protected $_attributes = [
        'type' => 'hidden',
    ];


    /**
     * @var TokenValidator
     */
    protected $_tokenValidator;

    protected $csrfValidatorOptions = [];


    /**
     * Accepted options for Csrf:
     * - csrf_options: an array used in the Csrf
     *
     * @param array|\Traversable $options
     * @return $this
     */
    public function setOptions($options)
    {
        parent::setOptions($options);

        if (isset($options['csrf_options'])) {
            $this->setCsrfValidatorOptions($options['csrf_options']);
        }

        return $this;
    }


    /**
     * @return array
     */
    public function getCsrfValidatorOptions()
    {
        return $this->csrfValidatorOptions;
    }


    /**
     * @param array $options
     * @return $this
     */
    public function setCsrfValidatorOptions(array $options)
    {
        $this->csrfValidatorOptions = $options;
        return $this;
    }


    /**
     * @return TokenValidator
     */
    public function getTokenValidator()
    {
        if (!$this->_tokenValidator) {
            $csrfOptions = $this->getCsrfValidatorOptions();
            $csrfOptions = array_merge($csrfOptions, ['name' => $this->getName()]);
            $this->setTokenValidator(new TokenValidator($csrfOptions));
        }
        return $this->_tokenValidator;
    }


    public function setTokenValidator($tokenValidator)
    {
        $this->_tokenValidator = $tokenValidator;
        return $this;
    }


    public function getValue()
    {
        if (!$this->_value) {
            $this->_value = $this->getTokenValidator()->getHash();
        }
        return $this->_value;
    }


    public function removeToken()
    {
        $this->getTokenValidator()->removeToken($this->getValue());
    }


    /**
     * Provide default input rules for this element
     *
     * Attaches the captcha as a validator.
     *
     * @return array
     */
    public function getInputSpecification()
    {
        return [
            'name'       => $this->getName(),
            'required'   => true,
            'filters'    => [
                ['name' => 'Zend\Filter\StringTrim'],
            ],
            'validators' => [
                $this->getTokenValidator(),
            ],
        ];
    }


}
