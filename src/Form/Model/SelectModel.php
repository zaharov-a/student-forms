<?php

namespace Ox3a\Form\Model;

use Zend\Validator;

class SelectModel extends ElementModel
{

    public function getInputSpecification()
    {
        $data = parent::getInputSpecification();

        $data['validators'][] = [
            'name'    => Validator\InArray::class,
            'options' => ['haystack' => $this->getHaystack()],
        ];

        return $data;
    }

    private function getHaystack()
    {
        $options  = $this->getOption('options') ?: [];
        $haystack = [];

        foreach ($options as $valueOrGroup => $textOrSubOptions) {
            if (is_array($textOrSubOptions)) {
                $haystack = array_merge($haystack, array_keys($textOrSubOptions));
            } else {
                $haystack[] = $valueOrGroup;
            }
        }

        return $haystack;
    }

}
