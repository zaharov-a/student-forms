<?php


namespace Ox3a\Form\Model;

use Zend\Filter;
use Zend\Validator;

class DefaultConfigsModel
{

    public static $arrayKeys = ['options', 'attributes', 'filters', 'validators'];


    public function getPhone($params = [], $label = '', $placeholder = '')
    {
        $params['type'] = ElementModel::class;

        foreach (self::$arrayKeys as $key) {
            if (!isset($params[$key])) {
                $params[$key] = [];
            }
        }

        if ($label) {
            $params['options']['label'] = $label;
        }

        if ($placeholder) {
            $params['attributes']['placeholder'] = $placeholder;
        }

        $params['attributes']['data-type'] = 'phone';

        return $params;
    }


    public function getEmail($params = [], $label = '', $placeholder = '')
    {
        $params['type'] = ElementModel::class;

        foreach (self::$arrayKeys as $key) {
            if (!isset($params[$key])) {
                $params[$key] = [];
            }
        }

        if ($label) {
            $params['options']['label'] = $label;
        }

        if ($placeholder) {
            $params['attributes']['placeholder'] = $placeholder;
        }

        // $params['attributes']['data-type'] = 'email';
        $params['attributes']['type'] = 'email';

        $params['validators'][] = ['name' => Validator\EmailAddress::class];

        return $params;
    }


    public function getRadioGroup($params = [], $label = '', $options = [])
    {
        $params['type'] = RadioGroupModel::class;

        foreach (self::$arrayKeys as $key) {
            if (!isset($params[$key])) {
                $params[$key] = [];
            }
        }

        $params['attributes']['required'] = true;

        if ($label) {
            $params['options']['label'] = $label;
        }

        if ($options) {
            $params['options']['options'] = $options;
        }

        return $params;
    }


    public function getRequiredDate($params = [], $label = '', $placeholder = '')
    {
        $params = $this->getDate($params, $label, $placeholder);

        $params['attributes']['required'] = true;

        return $params;
    }


    public function getDate($params = [], $label = '', $placeholder = '')
    {
        $params['type'] = ElementModel::class;

        foreach (self::$arrayKeys as $key) {
            if (!isset($params[$key])) {
                $params[$key] = [];
            }
        }

        $params['attributes']['type'] = 'date';
        $params['filters'][]          = ['name' => Filter\StringTrim::class];
        $params['validators'][]       = ['name' => Validator\Date::class, 'options' => ['format' => 'Y-m-d']];
        // $params['filters'][] = ['name' => Filter\HtmlEntities::class];


        if ($label) {
            $params['options']['label'] = $label;
        }

        if ($placeholder) {
            $params['attributes']['placeholder'] = $placeholder;
        }

        $params['attributes']['data-rule-date'] = true;

        return $params;
    }


    public function getRequiredText($params = [], $label = '', $placeholder = '')
    {
        $params = $this->getText($params, $label, $placeholder);

        $params['attributes']['required'] = true;

        return $params;
    }


    public function getText($params = [], $label = '', $placeholder = '')
    {
        $params['type'] = ElementModel::class;

        foreach (self::$arrayKeys as $key) {
            if (!isset($params[$key])) {
                $params[$key] = [];
            }
        }

        $params['filters'][] = ['name' => Filter\StringTrim::class];
        $params['filters'][] = ['name' => Filter\HtmlEntities::class];

        if ($label) {
            $params['options']['label'] = $label;
        }

        if ($placeholder) {
            $params['attributes']['placeholder'] = $placeholder;
        }

        return $params;
    }


    public function getSubmit($params = [], $title = '', $icon = '')
    {
        $params['type'] = ButtonModel::class;
        if (!isset($params['name'])) {
            $params['name'] = 'submit';
        }

        if (!isset($params['options'])) {
            $params['options'] = [];
        }

        if (!isset($params['options']['label'])) {
            $params['options']['label'] = '&nbsp;';
        }

        if ($title) {
            $params['options']['title'] = $title;
        }

        if ($icon) {
            $params['options']['icon'] = $icon;
        }

        if (!isset($params['options']['title'])) {
            $params['options']['title'] = 'Сохранить';
        }

        return $params;
    }


    public function getSeparator($name)
    {
        return [
            'type' => SeparatorModel::class,
            'name' => $name,
        ];
    }


    public function getToken($name)
    {
        return [
            'type' => TokenModel::class,
            'name' => $name,
        ];
    }


    public function getHidden($name, $validators = [])
    {
        return [
            'type'       => HiddenModel::class,
            'name'       => $name,
            'validators' => $validators,
        ];
    }


    public function getSelect($params = [], $label = '', $options = [])
    {
        $params['type'] = SelectModel::class;

        foreach (self::$arrayKeys as $key) {
            if (!isset($params[$key])) {
                $params[$key] = [];
            }
        }

        if ($label) {
            $params['options']['label'] = $label;
        }

        if ($options) {
            $params['options']['options'] = $options;
        }

        return $params;
    }


    public function getRequiredSelect($params = [], $label = '', $options = [])
    {
        $params = $this->getSelect($params, $label, $options);

        $params['attributes']['required'] = true;

        return $params;
    }


    public function getCheckbox($params = [], $label = '', $checkedValue = 1, $uncheckedValue = 0)
    {
        $params['type'] = CheckboxModel::class;

        foreach (self::$arrayKeys as $key) {
            if (!isset($params[$key])) {
                $params[$key] = [];
            }
        }

        $params['options']['checked_value']   = $checkedValue;
        $params['options']['unchecked_value'] = $uncheckedValue;

        if ($label) {
            $params['options']['label'] = $label;
        }

        return $params;
    }
}
