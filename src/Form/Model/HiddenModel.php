<?php


namespace Ox3a\Form\Model;


class HiddenModel extends ElementModel
{
    protected $_attributes = [
        'type' => 'hidden',
    ];

}
