<?php


namespace Ox3a\Form\Model;

use Zend\Validator;

class RadioGroupModel extends ElementModel
{

    public function getInputSpecification()
    {
        $data = parent::getInputSpecification();

        $options = $this->getOption('options') ?: [];

        $data['validators'][] = ['name' => Validator\InArray::class, 'options' => ['haystack' => array_keys($options)]];

        return $data;
    }
}
