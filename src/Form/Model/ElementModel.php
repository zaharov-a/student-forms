<?php


namespace Ox3a\Form\Model;


use InvalidArgumentException;
use JsonSerializable;
use Zend\Filter\AbstractFilter;
use Zend\Validator\AbstractValidator;

class ElementModel implements JsonSerializable
{
    const DEFAULT_LABEL_WIDTH = 'col-sm-2';

    const DEFAULT_ELEMENT_WIDTH = 'col-sm-10';

    /**
     * @var array
     */
    protected $_attributes = [
        'class' => 'form-control',
    ];

    /**
     * @var null|string
     */
    protected $_label;

    /**
     * @var array
     */
    protected $_labelAttributes = [];

    /**
     * Label specific options
     *
     * @var array
     */
    protected $_labelOptions = [
        'width' => self::DEFAULT_LABEL_WIDTH,
    ];

    /**
     * @var array Validation error messages
     */
    protected $_messages = [];

    /**
     * @var array custom options
     */
    protected $_options = [
        'width' => self::DEFAULT_ELEMENT_WIDTH,
    ];

    /**
     * @var mixed
     */
    protected $_value;

    /**
     * @var array
     */
    protected $_filters = [];

    /**
     * @var array
     */
    protected $_validators = [];


    /**
     * @param null|int|string $name       Optional name for the element
     * @param array           $options    Optional options for the element
     * @param array           $attributes Optional attributes for the element
     */
    public function __construct($name = null, $options = [], $attributes = [])
    {
        if (null !== $name) {
            $this->setName($name);
        }

        if (!empty($options)) {
            $this->setOptions($options);
        }

        if (!empty($attributes)) {
            $this->setAttributes($attributes);
        }
    }


    public function init()
    {
    }


    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }


    /**
     * @param array $attributes
     * @return $this
     */
    public function setAttributes($attributes)
    {
        $this->_attributes = [];
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
        return $this;
    }


    /**
     * @return string|null
     */
    public function getLabel()
    {
        return $this->_label;
    }


    /**
     * @param string|null $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->_label = $label;
        return $this;
    }


    /**
     * @return array
     */
    public function getLabelAttributes()
    {
        return $this->_labelAttributes;
    }


    /**
     * @param array $labelAttributes
     * @return $this
     */
    public function setLabelAttributes(array $labelAttributes)
    {
        $this->_labelAttributes = $labelAttributes;
        return $this;
    }


    /**
     * @return array
     */
    public function getLabelOptions()
    {
        return $this->_labelOptions;
    }


    /**
     * @param array $labelOptions
     * @return $this
     */
    public function setLabelOptions($labelOptions)
    {
        if (!is_array($labelOptions)) {
            throw new InvalidArgumentException('The options parameter must be an array');
        }

        foreach ($labelOptions as $key => $value) {
            $this->setLabelOption($key, $value);
        }

        return $this;
    }


    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->_messages;
    }


    /**
     * @param array $messages
     * @return $this
     */
    public function setMessages($messages)
    {
        $this->_messages = $messages;
        return $this;
    }


    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }


    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options)
    {
        if (!is_array($options)) {
            throw new InvalidArgumentException('The options parameter must be an array');
        }

        if (isset($options['label'])) {
            $this->setLabel($options['label']);
        }

        if (isset($options['label_attributes'])) {
            $this->setLabelAttributes($options['label_attributes']);
        }

        if (isset($options['label_options'])) {
            if (!isset($options['label_options']['width'])) {
                $options['label_options']['width'] = self::DEFAULT_LABEL_WIDTH;
            }
            $this->setLabelOptions($options['label_options']);
        }

        if (!isset($options['width'])) {
            $options['width'] = self::DEFAULT_ELEMENT_WIDTH;
        }

        $this->_options = $options;
        $this->_postSetOptions();
        return $this;
    }


    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }


    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->_value = $value;
        return $this;
    }


    public function setName($name)
    {
        $this->setAttribute('name', $name);
        $this->updateId();
        return $this;
    }


    public function getName()
    {
        return $this->getAttribute('name');
    }


    public function getFullName()
    {
        $group = $this->getOption('groupName');
        if (strlen((string)$group)) {
            return sprintf('%s[%s]', $group, $this->getName());
        }

        return $this->getName();
    }


    public function setOption($key, $value)
    {
        $this->_options[$key] = $value;
        $this->_postSetOption($key);
        return $this;
    }


    public function getOption($option)
    {
        if (!isset($this->_options[$option])) {
            return null;
        }

        return $this->_options[$option];
    }


    public function setAttribute($key, $value)
    {
        // Do not include the value in the list of attributes
        if ($key === 'value') {
            $this->setValue($value);
            return $this;
        }
        $this->_attributes[$key] = $value;
        return $this;
    }


    public function getAttribute($key)
    {
        if ($this->hasAttribute($key)) {
            return $this->_attributes[$key];
        }
        return null;
    }


    public function hasAttribute($key)
    {
        return array_key_exists($key, $this->_attributes);
    }


    public function setLabelOption($key, $value)
    {
        $this->_labelOptions[$key] = $value;
        return $this;
    }


    public function getLabelOption($key)
    {
        if ($this->hasLabelOption($key)) {
            return $this->_labelOptions[$key];
        }

        return null;
    }


    public function removeLabelOption($key)
    {
        unset($this->_labelOptions[$key]);
        return $this;
    }


    public function hasLabelOption($key)
    {
        return array_key_exists($key, $this->_labelOptions);
    }


    public function removeLabelOptions(array $keys)
    {
        foreach ($keys as $key) {
            unset($this->_labelOptions[$key]);
        }

        return $this;
    }


    public function clearLabelOptions()
    {
        $this->_labelOptions = [];
        return $this;
    }


    /**
     * @param bool $value
     * @return $this
     */
    public function setRequired($value)
    {
        return $this->setAttribute('required', $value);
    }


    public function isElementFirst()
    {
        return $this->getOption('element_first');
    }


    public function setElementFirst($value)
    {
        return $this->setOption('element_first', $value);
    }


    public function getInputSpecification()
    {
        return [
            'required'   => $this->getAttribute('required'),
            'filters'    => $this->_filters,
            'validators' => $this->_validators,
            'name'       => $this->getName(),
        ];
    }


    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->_filters;
    }


    /**
     * @param array $filters
     * @return ElementModel
     */
    public function setFilters($filters)
    {
        $this->_filters = $filters;
        return $this;
    }


    /**
     * @return array
     */
    public function getValidators()
    {
        return $this->_validators;
    }


    /**
     * @param array $validators
     * @return ElementModel
     */
    public function setValidators($validators)
    {
        $this->_validators = $validators;
        return $this;
    }


    /**
     * @param AbstractFilter $filter
     * @return $this
     */
    public function addFilter($filter)
    {
        $this->_filters[] = $filter;
        return $this;
    }


    /**
     * @param AbstractValidator $validator
     * @return $this
     */
    public function addValidator($validator)
    {
        $this->_validators[] = $validator;
        return $this;
    }


    public function jsonSerialize()
    {
        return [
            'name'            => $this->getName(),
            'value'           => $this->getValue(),
            'attributes'      => $this->getAttributes(),
            'options'         => $this->getOptions(),
            'label'           => $this->getLabel(),
            'labelAttributes' => $this->getLabelAttributes(),
            'labelOptions'    => $this->getLabelOptions(),
            'validators'      => array_values(
                array_filter(
                    $this->getValidators(),
                    function ($validator) {
                        return is_array($validator) || $validator instanceof JsonSerializable;
                    }
                )
            ),
            'filters'         => array_values(
                array_filter(
                    $this->getFilters(),
                    function ($filter) {
                        return is_array($filter) || $filter instanceof JsonSerializable;
                    }
                )
            ),
        ];
    }


    public function updateId()
    {
        $name = $this->getFullName();
        $name = preg_replace('/[^a-z0-9]+/i', '_', $name);
        $this->setAttribute('id', 'input_' . $name);
    }


    protected function _postSetOptions()
    {
        $this->updateId();
    }


    protected function _postSetOption($name)
    {
        if ($name == 'groupName') {
            $this->updateId();
        }
    }


}
