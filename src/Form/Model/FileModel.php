<?php


namespace Ox3a\Form\Model;


use Zend\InputFilter\FileInput;

class FileModel extends ElementModel
{
    protected $_attributes = [
        'type' => 'file',
    ];


    /**
     * @inheritDoc
     * @return array
     */
    public function getInputSpecification()
    {
        return [
            'type'       => FileInput::class,
            'required'   => $this->getAttribute('required'),
            'filters'    => $this->_filters,
            'validators' => $this->_validators,
        ];
    }
}
