<?php


namespace Ox3a\Form\Model;

use Ox3a\Form\GetFormValueInterface;
use Zend\InputFilter\InputFilter;

class FormModel extends GroupModel
{
    const ATTRIBUTE_ENCTYPE           = 'enctype';
    const ENCTYPE_MULTIPART_FORM_DATA = 'multipart/form-data';

    protected $_attributes = [
        'action' => '#',
        'method' => 'post',
        'class'  => 'form-horizontal',
    ];

    /**
     * @var DefaultConfigsModel
     */
    protected $_defaultConfigGenerator;


    /**
     * FormModel constructor.
     * @param null  $name
     * @param array $options
     * @param array $attributes
     */
    public function __construct($name = null, $options = [], $attributes = [])
    {
        parent::__construct($name, $options, $attributes);

        $this->init();
    }


    public function init()
    {
    }


    /**
     * @return DefaultConfigsModel
     * @deprecated
     */
    public function getDefaultConfigGenerator()
    {
        if (!$this->_defaultConfigGenerator) {
            $this->_defaultConfigGenerator = new DefaultConfigsModel();
        }
        return $this->_defaultConfigGenerator;
    }

    /**
     * @param GetFormValueInterface|array|null $value
     * @return FormModel|GroupModel
     */
    public function setValue($value)
    {
        if ($value instanceof GetFormValueInterface) {
            $value = $value->getFormValue();
        }

        return parent::setValue($value);
    }

}
