<?php


namespace Ox3a\Form\Model;


use InvalidArgumentException;
use Ox3a\Form\Factory\ElementFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class GroupModel extends ElementModel
{


    /**
     * @var ElementModel[]
     */
    protected $_elements = [];

    /**
     * @var string[]
     */
    protected $_attributes = [
        'class' => 'sub-form-horizontal',
    ];


    public function __clone()
    {
        $this->_elements = array_map(
            function ($element) {
                return clone $element;
            },
            $this->_elements
        );
    }


    public function createElement($data)
    {
        $element = ElementFactory::factory($data);

        return $element;
    }


    public function add($elementOrFieldset)
    {
        $element = $this->_prepareAppendData($elementOrFieldset);

        $element->setOption('groupName', $this->getFullName());
        $this->_appendElement($element);

        return $this;
    }


    /**
     * @return InputFilter
     */
    public function getInputSpecification()
    {
        $inputFilter = new InputFilter();

        foreach ($this->_elements as $element) {
            $inputFilter->add($element->getInputSpecification(), $element->getName());
        }

        return $inputFilter;
    }


    public function getElement($name)
    {
        if (isset($this->_elements[$name])) {
            return $this->_elements[$name];
        }

        return null;
    }


    public function getElements()
    {
        return $this->_elements;
    }


    public function setValue($value)
    {
        if (!$value) {
            $value = [];
        }

        foreach ($this->getElements() as $element) {
            if (array_key_exists($element->getName(), $value)) {
                $element->setValue($value[$element->getName()]);
            } else {
                $element->setValue(null);
            }
        }

        return $this;
    }


    public function getValue()
    {
        $data = [];

        foreach ($this->getElements() as $element) {
            if ($element instanceof StaticModel) {
                continue;
            }
            $data[$element->getName()] = $element->getValue();
        }

        return $data;
    }


    /**
     * @param array $data
     * @return $this
     */
    public function setData($data)
    {
        $this->setValue($data);

        return $this;
    }


    public function getData()
    {
        return $this->getValue();
    }


    public function isValid()
    {
        $inputFilter = $this->getInputSpecification();
        $inputFilter->setData($this->getValue());
        $isValid = $inputFilter->isValid();

        $this->setValue($inputFilter->getValues());

        $this->setMessages($inputFilter->getMessages());

        return $isValid;
    }


    public function getMessages()
    {
        $messages = [];

        foreach ($this->getElements() as $element) {
            $messages[$element->getName()] = $element->getMessages();
        }

        return $messages;
    }


    public function setMessages($errorMessages)
    {
        foreach ($this->_elements as $element) {
            $element->setMessages(
                isset($errorMessages[$element->getName()]) ? $errorMessages[$element->getName()] : []
            );
        }
    }


    public function getMessage()
    {
        $errors = $this->getMessages();
        $msg    = [];

        foreach ($errors as $name => $messages) {
            $msg[] = sprintf('<b>%s</b>:', $this->getElement($name)->getLabel());
            $msg[] = implode('<br>', $messages);
        }

        return implode('<br>', $msg);
    }


    public function jsonSerialize()
    {
        $data = parent::jsonSerialize();

        $data['elements'] = [];

        foreach ($this->_elements as $elementModel) {
            $data['elements'][] = $elementModel->jsonSerialize();
        }

        return $data;
    }


    /**
     * @param ElementModel|array $elementOrFieldset
     * @return ElementModel
     */
    protected function _prepareAppendData($elementOrFieldset)
    {
        if (is_array($elementOrFieldset)) {
            $element = $this->createElement($elementOrFieldset);
        } else {
            $element = $elementOrFieldset;
        }

        if (!($element instanceof ElementModel)) {
            throw new InvalidArgumentException(
                'Ожидался массив или ElementModel, но пришло :'
                . (is_object($elementOrFieldset) ? get_class($elementOrFieldset) : gettype($elementOrFieldset))
            );
        }

        return $element;
    }


    /**
     * @param ElementModel $element
     */
    protected function _appendElement(ElementModel $element)
    {
        $this->_elements[$element->getName()] = $element;
    }


    protected function _postSetOption($name)
    {
        parent::_postSetOption($name);
        if ($name == 'groupName') {
            foreach ($this->_elements as $element) {
                $element->setOption('groupName', $this->getFullName());
            }
        }
    }


    protected function _postSetOptions()
    {
        parent::_postSetOptions();
        foreach ($this->_elements as $element) {
            $element->setOption('groupName', $this->getFullName());
        }
    }
}
