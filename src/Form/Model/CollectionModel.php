<?php


namespace Ox3a\Form\Model;


use Zend\InputFilter\CollectionInputFilter;

class CollectionModel extends GroupModel
{

    /**
     * @var ElementModel[][]
     */
    protected $_collection = [];


    public function __clone()
    {
    }


    public function add($elementOrFieldset)
    {
        $element = $this->_prepareAppendData($elementOrFieldset);
        $this->_appendElement($element);

        return $this;
    }


    public function setValue($collectionData)
    {
        if (!$collectionData) {
            $collectionData = [];
        }

        $this->_collection = [];

        foreach ($collectionData as $i => $data) {
            $row = [];
            foreach ($this->getElements() as $element) {
                $rowElement = clone $element;
                $value      = isset($data[$element->getName()]) ? $data[$element->getName()] : null;

                $rowElement->setValue($value);

                $rowElement->setOption('groupName', $this->getFullName() . "[{$i}]");

                $row[$rowElement->getName()] = clone $rowElement;
            }
            $this->_collection[] = $row;
        }

        return $this;
    }


    public function getValue()
    {
        $value = [];

        foreach ($this->getCollection() as $list) {
            $row = [];
            foreach ($list as $element) {
                if ($element instanceof StaticModel) {
                    continue;
                }
                $row[$element->getName()] = $element->getValue();
            }
            $value[] = $row;
        }
        return $value;
    }


    public function setData($collectionData)
    {
        return $this->setValue($collectionData);
    }


    public function setMessages($errorMessages)
    {
        foreach ($this->getCollection() as $index => $row) {
            foreach ($row as $element) {
                $element->setMessages(
                    isset($errorMessages[$index][$element->getName()]) ? $errorMessages[$index][$element->getName(
                    )] : []
                );
            }
        }
    }


    public function getMessages()
    {
        $messages = [];
        foreach ($this->getCollection() as $index => $row) {
            $row = [];
            foreach ($row as $element) {
                $row[$element->getName()] = $element->getMessages();
            }
            $messages[] = $row;
        }

        return $messages;
    }


    public function getCollection()
    {
        return $this->_collection;
    }


    public function getInputSpecification()
    {
        $inputFilter = new CollectionInputFilter();

        foreach ($this->getElements() as $element) {
            $inputFilter->getInputFilter()->add($element->getInputSpecification(), $element->getName());
        }

        return $inputFilter;
    }
}
