<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


class SeparatorHelper extends AbstractHelper
{
    public function render()
    {
        return '<hr>';
    }

}
