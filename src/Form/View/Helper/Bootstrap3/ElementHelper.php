<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use InvalidArgumentException;
use Ox3a\Form\Model\ElementModel;

class ElementHelper extends AbstractHelper
{
    /**
     * Attributes valid for the input tag
     *
     * @var array
     */
    protected $_validTagAttributes = [
        'name'           => true,
        'accept'         => true,
        'alt'            => true,
        'autocomplete'   => true,
        'autofocus'      => true,
        'checked'        => true,
        'dirname'        => true,
        'disabled'       => true,
        'form'           => true,
        'formaction'     => true,
        'formenctype'    => true,
        'formmethod'     => true,
        'formnovalidate' => true,
        'formtarget'     => true,
        'height'         => true,
        'list'           => true,
        'max'            => true,
        'maxlength'      => true,
        'min'            => true,
        'multiple'       => true,
        'pattern'        => true,
        'placeholder'    => true,
        'readonly'       => true,
        'required'       => true,
        'size'           => true,
        'src'            => true,
        'step'           => true,
        'type'           => true,
        'value'          => true,
        'width'          => true,
    ];

    /**
     * Valid values for the input type
     *
     * @var array
     */
    protected $_validTypes = [
        'text'     => true,
        'button'   => true,
        'checkbox' => true,
        'file'     => true,
        'hidden'   => true,
        // 'image'          => true,
        'password' => true,
        'radio'    => true,
        'reset'    => true,
        // 'select'         => true,
        'submit'   => true,
        // 'color'          => true,
        'date'           => true,
        // 'datetime'       => true,
        // 'datetime-local' => true,
        'email'    => true,
        // 'month'          => true,
        'number'         => true,
        // 'range'          => true,
        // 'search'         => true,
        // 'tel'            => true,
        // 'time'           => true,
        // 'url'            => true,
        // 'week'           => true,
    ];


    /**
     * Render a form <input> element from the provided $element
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function render()
    {
        $element  = $this->getElement();
        $name     = $element->getName();
        $fullName = $element->getFullName();
        if ($name === null || $name === '') {
            throw new InvalidArgumentException(
                sprintf(
                    '%s requires that the element has an assigned name; none discovered',
                    __METHOD__
                )
            );
        }

        $attributes          = $element->getAttributes();
        $attributes['name']  = $fullName;
        $type                = $this->getType($element);
        $attributes['type']  = $type;
        $attributes['value'] = $element->getValue();

        if ('password' == $type) {
            $attributes['value'] = '';
        }

        $icon = $element->getOption('icon');

        if ($icon) {
            return sprintf(
                '<div class="input-group"><input %s><span class="input-group-addon">%s</span></div>',
                $this->createAttributesString($attributes),
                $this->renderIcon($icon)
            );
        }

        return sprintf(
            '<input %s>',
            $this->createAttributesString($attributes)
        );
    }


    /**
     * Determine input type to use
     *
     * @param ElementModel $element
     * @return string
     */
    protected function getType(ElementModel $element)
    {
        $type = $element->getAttribute('type');
        if (empty($type)) {
            return 'text';
        }

        $type = strtolower($type);
        if (!isset($this->_validTypes[$type])) {
            return 'text';
        }

        return $type;
    }


    /**
     * Create a string of all attribute/value pairs
     *
     * Escapes all attribute values
     *
     * @param array $attributes
     * @return string
     */
    public function createAttributesString(array $attributes)
    {
        // $attributes = $this->prepareAttributes($attributes);
        $strings = [];

        foreach ($attributes as $key => $value) {
            $key = strtolower($key);

            if (!$value && isset($this->_booleanAttributes[$key])) {
                // Skip boolean attributes that expect empty string as false value
                if ('' === $this->_booleanAttributes[$key]['off']) {
                    continue;
                }
            }

            //check if attribute is translatable and translate it
            $value = $this->translateHtmlAttributeValue($key, $value);

            $strings[] = sprintf('%s="%s"', $this->escapeHtml($key), $this->escapeHtmlAttr($value));
        }

        return implode(' ', $strings);
    }


    public function escapeHtml($value)
    {
        return $value;
    }


    public function escapeHtmlAttr($value)
    {
        return $value;
    }


    public function translateHtmlAttributeValue($key, $value)
    {
        if ($this->getElement()->getOption('escapeAttr')) {
            return htmlentities($value);
        }
        return $value;
    }


    public function renderIcon($icon)
    {
        return $icon ? sprintf('<i class="%s"></i> ', $icon) : '';
    }
}
