<?php

namespace Ox3a\Form\View\Helper\Bootstrap3;

use ReflectionException;

class FormRowHelper extends AbstractHelper
{

    public function render()
    {
        $element = $this->_element;
        $label   = $element->getLabel();
        $type    = $element->getAttribute('type');

        if (!empty($label) && $type !== 'hidden') {
            $labelMarkup   = $this->renderLabel();
            $elementMarkup = sprintf(
                '<div class="%s">%s%s%s</div>',
                $element->getOption('width'),
                $this->renderElement(),
                $this->renderDescription(),
                $this->renderMessages()
            );

            $template = '<div class="form-group %s">%s%s</div>';
            $hasError = $element->getMessages() ? 'has-error' : '';

            if ($element->isElementFirst()) {
                return sprintf(
                    $template,
                    $hasError,
                    $elementMarkup,
                    $labelMarkup
                );
            }

            return sprintf(
                $template,
                $hasError,
                $labelMarkup,
                $elementMarkup
            );
        } else {
            return $this->renderElement();
        }
        //     return sprintf('<div class="form-group">
        //     <label for="%s" class="control-label %s">%s</label>
        //     <div class="col-sm-10">
        //         <input type="text" name="name" id="inputname" class="form-control" value="&quot;А&quot;" placeholder="A, B, C" required="required" aria-required="true" aria-invalid="false">
        //         <small>через запятую</small>
        //     </div>
        // </div>');
    }


    public function renderLabel()
    {
        $element   = $this->getElement();
        $label     = $element->getLabel();
        $id        = $element->getAttribute('id');
        $classList = ['control-label', $element->getLabelOption('width')];
        if ($element->isElementFirst()) {
            $classList[] = 'control-label_left';
        }
        return sprintf('<label for="%s" class="%s">%s</label>', $id, implode(' ', $classList), $label);
    }


    public function renderMessages()
    {
        if (!$this->_element->getMessages()) {
            return '';
        }

        $result = '';
        foreach ($this->_element->getMessages() as $value => $messages) {
            if (is_array($messages)) {
                $message = "{$value}: ";
                if (count($messages) > 1) {
                    $message .= '<br>';
                }
                foreach ($messages as $valueMessage) {
                    $message .= "{$valueMessage}<br>";
                }
            } else {
                $message = $messages;
            }
            $result .= "$message<br>";
        }
        return sprintf('<span class="help-block">%s</span>', $result);
    }


    public function renderDescription()
    {
        $description = $this->getElement()->getOption('description');
        if (!$description) {
            return '';
        }
        return sprintf('<span class="help-block">%s</span>', $description);
    }


    /**
     * @return AbstractHelper
     * @throws ReflectionException
     */
    public function getElementRender()
    {
        return Factory::factory($this->getElement());
    }


    public function renderElement()
    {
        return $this->getElementRender()->setElement($this->_element)->render();
    }
}
