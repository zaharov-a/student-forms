<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use Ox3a\Form\Model\ElementModel;

abstract class AbstractHelper
{

    /**
     * Standard boolean attributes, with expected values for enabling/disabling
     *
     * @var array
     */
    protected $_booleanAttributes = [
        'autofocus' => ['on' => 'autofocus', 'off' => ''],
        'checked'   => ['on' => 'checked', 'off' => ''],
        'disabled'  => ['on' => 'disabled', 'off' => ''],
        'multiple'  => ['on' => 'multiple', 'off' => ''],
        'readonly'  => ['on' => 'readonly', 'off' => ''],
        'required'  => ['on' => 'required', 'off' => ''],
        'selected'  => ['on' => 'selected', 'off' => ''],
    ];

    protected $_view;

    /**
     * @var ElementModel
     */
    protected $_element;


    /**
     * @return string
     */
    abstract public function render();


    /**
     * @return ElementModel
     */
    public function getElement()
    {
        return $this->_element;
    }


    /**
     * @param ElementModel $element
     * @return AbstractHelper
     */
    public function setElement($element)
    {
        $this->_element = $element;
        return $this;
    }


    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }


    public function __toString()
    {
        return $this->render();
    }


    public function __invoke($element = null)
    {
        if ($element) {
            $this->setElement($element);
        }

        return $this;
    }


}
