<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use Ox3a\Form\Model\ElementModel;
use ReflectionClass;
use ReflectionException;

class Factory
{
    /**
     * @var Factory
     */
    protected static $_instance;

    /**
     * Карта вьюшек по моделям
     * @var string[]
     */
    protected $_map = [];


    /**
     * @return Factory
     */
    public static function getInstance()
    {
        return self::$_instance ?: (self::$_instance = new self());
    }


    /**
     * @param ElementModel $element
     * @return AbstractHelper
     * @throws ReflectionException
     */
    public static function factory($element)
    {
        return self::getInstance()->getHelper($element);
    }


    /**
     * @param ElementModel $element
     * @return AbstractHelper
     * @throws ReflectionException
     */
    public function getHelper($element)
    {
        $reflection = new ReflectionClass($element);
        if (!isset($this->_map[$reflection->getName()])) {
            $this->assignHelper($reflection->getName(), 'Ox3a\\Form\\View\\Helper\\Bootstrap3\\' . str_replace('Model', 'Helper', (new ReflectionClass($element))->getShortName()));
        }

        $class = $this->_map[$reflection->getName()];

        return new $class();
    }


    /**
     * Ассоциировать класс модели с классом хелпера
     * @param $elementClassName
     * @param $helperClassName
     */
    public function assignHelper($elementClassName, $helperClassName)
    {
        $this->_map[$elementClassName] = $helperClassName;
    }
}
