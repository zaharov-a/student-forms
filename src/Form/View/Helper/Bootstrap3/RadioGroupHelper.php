<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use InvalidArgumentException;

class RadioGroupHelper extends ElementHelper
{

    public function render()
    {
        $element  = $this->getElement();
        $name     = $element->getName();
        $fullName = $element->getFullName();
        if ($name === null || $name === '') {
            throw new InvalidArgumentException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $attributes         = $element->getAttributes();
        $attributes['name'] = $fullName;

        return sprintf(
            '<div class="btn-group" data-toggle="buttons">%s</div>',
            $this->renderOptions()
        );
    }


    public function renderOptions()
    {
        $btnTpl = '<label class="btn btn-default %s"><input %s type="radio" name="%s" value="%s">%s</label>';

        $result = '';

        $options = $this->getElement()->getOption('options') ?: [];
        $name    = $this->getElement()->getFullName();
        $current = $this->getElement()->getValue();

        foreach ($options as $value => $text) {
            $result .= sprintf(
                $btnTpl,
                ($current == $value) ? 'active' : '',
                ($current == $value) ? 'checked' : '',
                $name,
                $value,
                $text
            );
        }

        return $result;
    }
}
