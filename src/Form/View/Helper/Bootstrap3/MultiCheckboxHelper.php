<?php

namespace Ox3a\Form\View\Helper\Bootstrap3;

use InvalidArgumentException;

class MultiCheckboxHelper extends ElementHelper
{
    public function render()
    {
        $element  = $this->getElement();
        $name     = $element->getName();
        $fullName = $element->getFullName();
        if ($name === null || $name === '') {
            throw new InvalidArgumentException(
                sprintf(
                    '%s requires that the element has an assigned name; none discovered',
                    __METHOD__
                )
            );
        }

        $attributes         = $element->getAttributes();
        $attributes['name'] = $fullName;

        return sprintf(
            '<div class="btn-group" data-toggle="buttons">%s</div>',
            $this->renderOptions()
        );
    }


    public function renderOptions()
    {
        $id = $this->getElement()->getAttribute('id');

        $result = '';

        $options = $this->getElement()->getOption('options') ?: [];
        $name    = $this->getElement()->getFullName();
        $current = $this->getElement()->getValue() ?: [];

        $tpl = '
            <div class="custom-control custom-checkbox">
                <label>
                  <input type="checkbox" class="custom-control-input" name="%s[]" value="%s" %s>
                  %s
                </label>
            </div>';

        foreach ($options as $value => $text) {
            $result .= sprintf(
                $tpl,
                $name,
                $value,
                (in_array($value, $current)) ? 'checked' : '',
                $text
            );
        }

        return $result;
    }
}
