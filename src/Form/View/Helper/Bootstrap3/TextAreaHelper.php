<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;

use InvalidArgumentException;


class TextAreaHelper extends ElementHelper
{

    public function render()
    {
        $element  = $this->getElement();
        $name     = $element->getName();
        $fullName = $element->getFullName();
        if ($name === null || $name === '') {
            throw new InvalidArgumentException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $attributes         = $element->getAttributes();
        $attributes['name'] = $fullName;

        return sprintf(
            '<textarea %s>%s</textarea>',
            $this->createAttributesString($attributes),
            $element->getValue()
        );
    }

}
