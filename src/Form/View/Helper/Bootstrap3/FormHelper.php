<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use Ox3a\Form\Model\FormModel;

class FormHelper extends GroupHelper
{

    /**
     * @var FormModel
     */
    public $_element;


    public function render()
    {
        $form       = $this->getElement();
        $attributes = $form->getAttributes();

        return sprintf('<form %s>%s</form>', $this->createAttributesString($attributes), $this->renderElements());
    }

}
