<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;

use InvalidArgumentException;

class ButtonHelper extends ElementHelper
{

    public function render()
    {
        $element = $this->getElement();
        $name    = $element->getName();
        if ($name === null || $name === '') {
            throw new InvalidArgumentException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $attributes          = $element->getAttributes();
        $attributes['name']  = $name;
        $type                = $this->getType($element);
        $attributes['type']  = $type;
        $attributes['value'] = $element->getValue();
        $icon                = $element->getOption('icon');

        return sprintf(
            '<button class="btn btn-%s" %s>%s%s</button>',
            $element->getOption('type') ?: 'primary',
            $this->createAttributesString($attributes),
            $this->renderIcon($icon),
            $element->getOption('title')
        );
    }

}
