<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use InvalidArgumentException;

class CheckboxHelper extends ElementHelper
{
    /**
     * Attributes valid for the input tag
     *
     * @var array
     */
    protected $_validTagAttributes = [
        'name'           => true,
        // 'accept'         => true,
        // 'alt'            => true,
        'autocomplete'   => true,
        // 'autofocus'      => true,
        'checked'        => true,
        // 'dirname'        => true,
        'disabled'       => true,
        'form'           => true,
        'formaction'     => true,
        'formenctype'    => true,
        'formmethod'     => true,
        'formnovalidate' => true,
        'formtarget'     => true,
        // 'height'         => true,
        // 'list'           => true,
        // 'max'            => true,
        // 'maxlength'      => true,
        // 'min'            => true,
        // 'multiple'       => true,
        // 'pattern'        => true,
        // 'placeholder'    => true,
        'readonly'       => true,
        'required'       => true,
        // 'size'           => true,
        // 'src'            => true,
        // 'step'           => true,
        'type'           => true,
        'value'          => true,
        // 'width'          => true,
    ];


    public function render()
    {
        $element  = $this->getElement();
        $name     = $element->getName();
        $fullName = $element->getFullName();
        if ($name === null || $name === '') {
            throw new InvalidArgumentException(
                sprintf(
                    '%s requires that the element has an assigned name; none discovered',
                    __METHOD__
                )
            );
        }

        $attributes          = $element->getAttributes();
        $attributes['name']  = $fullName;
        $type                = $this->getType($element);
        $attributes['type']  = $type;
        $attributes['value'] = $element->getOption('checked_value');

        return sprintf(
            '%s<input %s>',
            $this->renderHidden($fullName, $element->getOption('unchecked_value')),
            $this->createAttributesString($attributes)
        );
    }


    public function renderHidden($name, $value)
    {
        return sprintf('<input type="hidden" name="%s" value="%s">', $name, $value);
    }

}
