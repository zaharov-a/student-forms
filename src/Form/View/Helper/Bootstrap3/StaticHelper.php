<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use Ox3a\Form\Model\StaticModel;

class StaticHelper extends ElementHelper
{

    public function render()
    {
        /** @var StaticModel $element */
        $element    = $this->getElement();
        $attributes = $element->getAttributes();
        unset($attributes['value']);

        return sprintf(
            '<p %s>%s</p>',
            $this->createAttributesString($attributes),
            $element->getValue()
        );
    }
}
