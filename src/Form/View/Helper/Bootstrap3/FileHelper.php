<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;

use InvalidArgumentException;


class FileHelper extends ElementHelper
{
    /**
     * @inheritDoc
     * @param string $key
     * @param mixed  $value
     * @return string
     */
    public function translateHtmlAttributeValue($key, $value)
    {
        if ($key == 'value') {
            return '';
        }
        return $value;
    }
}
