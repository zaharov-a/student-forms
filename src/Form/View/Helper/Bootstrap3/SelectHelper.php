<?php

namespace Ox3a\Form\View\Helper\Bootstrap3;

use InvalidArgumentException;

class SelectHelper extends ElementHelper
{

    /**
     * Attributes valid for the input tag
     *
     * @var array
     */
    protected $_validTagAttributes = [
        'name'           => true,
        'accept'         => true,
        'alt'            => true,
        'autocomplete'   => true,
        'autofocus'      => true,
        // 'checked'        => true,
        'dirname'        => true,
        'disabled'       => true,
        'form'           => true,
        'formaction'     => true,
        'formenctype'    => true,
        'formmethod'     => true,
        'formnovalidate' => true,
        'formtarget'     => true,
        // 'height'         => true,
        // 'list'           => true,
        // 'max'            => true,
        // 'maxlength'      => true,
        // 'min'            => true,
        'multiple'       => true,
        // 'pattern'        => true,
        // 'placeholder'    => true,
        'readonly'       => true,
        'required'       => true,
        'size'           => true,
        // 'src'            => true,
        // 'step'           => true,
        // 'type'           => true,
        // 'value'          => true,
        // 'width'          => true,
    ];

    public function render()
    {
        $element  = $this->getElement();
        $name     = $element->getName();
        $fullName = $element->getFullName();
        if ($name === null || $name === '') {
            throw new InvalidArgumentException(
                sprintf(
                    '%s requires that the element has an assigned name; none discovered',
                    __METHOD__
                )
            );
        }

        $attributes         = $element->getAttributes();
        $attributes['name'] = $fullName;

        return sprintf(
            '<select %s>%s</select>',
            $this->createAttributesString($attributes),
            $this->renderOptions($element->getOption('options'), $element->getValue())
        );
    }

    public function renderOptions($options, $selectedValue)
    {
        return implode(
            '',
            array_map(
                function ($valueOrGroup, $textOrSubOptions) use ($selectedValue) {
                    if (is_array($textOrSubOptions)) {
                        return $this->renderOptionGroup($valueOrGroup, $textOrSubOptions, $selectedValue);
                    }

                    return $this->renderOption(
                        $valueOrGroup,
                        $textOrSubOptions,
                        (string)$valueOrGroup == (string)$selectedValue
                    );
                },
                array_keys($options),
                $options
            )
        );
    }

    /**
     * @param string $group
     * @param array $options
     * @param string $selectedValue
     * @return string
     */
    private function renderOptionGroup($group, $options, $selectedValue)
    {
        $o = '';
        foreach ($options as $value => $text) {
            $o .= $this->renderOption($value, $text, (string)$value == (string)$selectedValue);
        }
        return sprintf(
            '<optgroup label="%s">%s</optgroup>',
            $this->translateHtmlAttributeValue('', $group),
            $o
        );
    }

    /**
     * @param string|int $value
     * @param string $text
     * @param bool $selected
     * @return string
     */
    private function renderOption($value, $text, $selected)
    {
        return sprintf(
            '<option value="%s"%s>%s</option>',
            $this->translateHtmlAttributeValue('', $value),
            $selected ? ' selected="selected"' : '',
            $this->translateHtmlAttributeValue('', $text)
        );
    }

}
