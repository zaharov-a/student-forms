<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use Ox3a\Form\Model\CollectionModel;

class CollectionHelper extends GroupHelper
{
    public function render()
    {
        /** @var CollectionModel $element */
        $element = $this->getElement();
        $head    = '';
        $body    = '';

        $headValue = false;

        foreach ($element->getElements() as $item) {
            $head      .= '<th>' . $item->getLabel() . '</th>';
            $headValue = $headValue || (bool)$item->getLabel();
        }

        if ($headValue) {
            $head = "<thead><tr>{$head}</tr></thead>";
        } else {
            $head = '';
        }

        foreach ($element->getCollection() as $i => $row) {
            $string = '';
            foreach ($row as $item) {
                $string .= sprintf(
                    '<td class="collection__col-%s">%s</td>',
                    $item->getName(),
                    $this->renderElement($item)
                );
            }

            $body .= '<tr>' . $string . '</tr>';
        }

        $markup = sprintf(
            '<table class="table collection_%s">
%s
<tbody>%s</tbody>
</table>',
            $element->getName(),
            $head,
            $body
        );

        if ($element->getLabel()) {
            return sprintf(
                '<div class="form-group">%s<div class="%s">%s</div></div>',
                $this->renderLabel(),
                $element->getOption('width'),
                $markup
            );
        }

        return $markup;
    }


    public function renderLabel()
    {
        $element   = $this->getElement();
        $label     = $element->getLabel();
        $id        = $element->getAttribute('id');
        $classList = ['control-label', $element->getLabelOption('width')];
        if ($element->isElementFirst()) {
            $classList[] = 'control-label_left';
        }
        return sprintf('<label for="%s" class="%s">%s</label>', $id, implode(' ', $classList), $label);
    }
}
