<?php


namespace Ox3a\Form\View\Helper\Bootstrap3;


use Ox3a\Form\Model\GroupModel;

class GroupHelper extends ElementHelper
{
    public function render()
    {
        $form       = $this->getElement();
        $attributes = $form->getAttributes();
        $title      = $form->getLabel() ? sprintf(
            '<div class="sub-form__label">%s</div>',
            $form->getLabel()
        ) : '';
        $elements   = $this->renderElements();
        return sprintf('<div %s>%s</div>', $this->createAttributesString($attributes), $title . $elements);
    }


    public function renderElements()
    {
        $rowHelper = new FormRowHelper();
        $result    = '';

        foreach ($this->_element->getElements() as $element) {
            switch (true) {
                case $element instanceof GroupModel:
                    $result .= $this->renderElement($element);
                    break;

                default:
                    $result .= $rowHelper($element);
            }
        }

        return $result;
    }


    public function renderElement($element)
    {
        return Factory::getInstance()->getHelper($element)->setElement($element)->render();
    }

}
