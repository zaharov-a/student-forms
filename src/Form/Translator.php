<?php

namespace Ox3a\Form;

use Zend\I18n\Translator\Resources;
use Zend\Validator\Translator\TranslatorInterface;

class Translator implements TranslatorInterface
{

    protected $_validate;

    public function __construct()
    {
        $this->_validate = include Resources::getBasePath() . sprintf(Resources::getPatternForValidator(), 'ru');
    }

    public function translate($message, $textDomain = 'default', $locale = null)
    {
        if (isset($this->_validate[$message])) {
            return $this->_validate[$message];
        }
        return $message;
    }

}
