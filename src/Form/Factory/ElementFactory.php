<?php


namespace Ox3a\Form\Factory;


use Ox3a\Form\Model\ElementModel;

class ElementFactory
{
    /**
     * @param $data
     * @return ElementModel
     */
    public static function factory($data)
    {
        $class = isset($data['type']) ? $data['type'] : ElementModel::class;
        /** @var ElementModel $element */
        $element = new $class();

        $element->setName($data['name']);

        if (isset($data['attributes'])) {
            foreach ($data['attributes'] as $key => $value) {
                $element->setAttribute($key, $value);
            }
        }

        if (isset($data['options'])) {
            $element->setOptions($data['options']);
        }

        if (isset($data['filters'])) {
            $element->setFilters(array_merge($element->getFilters(), $data['filters']));
        }

        if (isset($data['validators'])) {
            $element->setValidators(array_merge($element->getValidators(), $data['validators']));
        }

        // echo $element->getName();
        return $element;
    }

}
