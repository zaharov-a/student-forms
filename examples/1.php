<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <?php

    use Zend\Filter;
    use Zend\Validator;
    use Ox3a\Form\Model;

    require_once __DIR__ . '/../vendor/autoload.php';

    $configGenerator = new Model\DefaultConfigsModel();

    $form = new Model\FormModel();
    $form
        ->add($configGenerator->getToken('token'))
        ->add($configGenerator->getHidden('id', [['name' => Validator\Digits::class]]))
        ->add(
            $configGenerator->getRequiredText(
                [
                    'name'    => 'title',
                    'options' => [
                        'element_first' => true,
                    ],

                ],
                'Название',
                'Новый филиал'
            )
        )
        ->add($configGenerator->getRequiredText(['name' => 'address'], 'Адрес', 'г.Перевоз'))
        ->add($configGenerator->getSelect(['name' => 'select'], 'Select', ['1', '2', '3']))
        ->add($configGenerator->getCheckbox(['name' => 'checkbox'], 'Checkbox'))
        ->add($configGenerator->getSubmit([], 'сохранить'));

    $form->setData(
        [
            'select'   => 1,
            'checkbox' => 1,
        ]
    );

    $view = new \Ox3a\Form\View\Helper\Bootstrap3\FormHelper();


    if (!empty($_POST)) {
        $form->setData($_POST);
        ?>
        <pre>
<?php print_r($_POST) ?>
</pre>
        <?php
    }
    echo $view($form);
    ?>
</div>
</body>
</html>
